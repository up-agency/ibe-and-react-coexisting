import React from 'react';
import './App.css';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  return (
  <div className="App">
    <Router>
      <div>
        <nav>
           <Link to="/">Home</Link>
           <Link to="/about">About</Link>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  </div>
  );
}


class Home extends React.Component {
    componentDidMount(){
      window.up.bootstrapEngine()
    }

    componentWillUnmount(){
      window.up.destroyEngine()
    }

    render(){
      return (
      <div>
        <h1>Home</h1>
        <ibe-up ibe-key="75c8bc0d-9dba-4828-9d58-e7ad334b6795" language="en"></ibe-up>
      </div>
      );
    }
}

class About extends React.Component {
  componentDidMount(){
    window.up.bootstrapEngine()
  }

  componentWillUnmount(){
    window.up.destroyEngine()
  }


  render(){
      return (
      <div>
        <h1>About</h1>
        <ibe-up ibe-key="75c8bc0d-9dba-4828-9d58-e7ad334b6795" language="en"></ibe-up>
      </div>
      );
  }
}


export default App;
